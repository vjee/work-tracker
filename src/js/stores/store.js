import {observable, action} from "mobx";
import moment from "moment";

import {save as saveData, get as getData} from "./../lib/storage";
import constructSaveDate from "./../lib/constructSaveDate";

class Store {
  @observable tracks = [];

  @observable startDate = moment();
  @observable endDate = moment();

  startHour;
  startMinutes;
  endHour;
  endMinutes;

  constructor() {
    this.get();
  }

  @action
  save = () => {
    const start = constructSaveDate(
      this.startDate,
      this.startHour,
      this.startMinutes
    );
    const end = constructSaveDate(this.endDate, this.endHour, this.endMinutes);

    const data = {
      start: start.valueOf(),
      end: end.valueOf(),
      description: `description`
    };

    const key = String(Date.now());

    saveData(key, data)
      .then(() => this._add({key, value: data}))
      .catch(err => console.error(err));
  };

  @action
  _add = data => {
    this.tracks.push(data);
  };

  @action
  get = () => {
    const handler = (value, key) => {
      this._add({key, value});
    };

    getData(handler)
      .then(() => console.log(`success`))
      .catch(() => console.error(`error`));
  };

  @action
  set = (value, target) => {
    this[target] = value;
  };
}

const store = new Store();

if (process.env.NODE_ENV !== `production`) {
  window.store = store;
}

export default store;
