export default millis => {
  let date;
  if (millis instanceof Date) date = millis;
  else date = new Date(millis);

  const hours = date.getUTCHours();
  const minutes = date.getUTCMinutes();

  return {hours, minutes};
};
