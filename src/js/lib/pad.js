export default value => (parseInt(value) < 10 ? `0${value}` : value);
