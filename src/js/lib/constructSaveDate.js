import createDateTime from "./createDateTime";
import parseMomentDate from "./parseMomentDate";
import pad from "./pad";

export default (date, hour, minutes) => {
  const {year, month, day} = parseMomentDate(date);
  const h = pad(hour || 0);
  const m = pad(minutes || 0);

  return createDateTime(year, month, day, h, m);
};
