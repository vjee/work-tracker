// date will be the moment format date
export default date => {
  const d = date.format(`YYYY:MM:DD`).split(`:`);
  return {year: d[0], month: d[1], day: d[2]};
};
