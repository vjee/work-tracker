import moment from "moment";

export default (year, month, day, hours, minutes) => {
  const dateString = `${year}-${month}-${day} ${hours}:${minutes}`;
  return moment(dateString);
};
