import React from "react";
import {func, object} from "prop-types";
import {inject, observer} from "mobx-react";

import TimeInputRow from "./TimeInputRow";

const Insert = ({save, startDate, endDate}) =>
  <section className='insert'>
    <TimeInputRow
      title='From:'
      date={startDate}
      target='start'
      placeholder={[11, 0]}
    />
    <TimeInputRow
      title='Till:'
      date={endDate}
      target='end'
      placeholder={[21, 45]}
    />
    <button onClick={save}>Save</button>
  </section>;

Insert.propTypes = {
  save: func.isRequired,
  startDate: object.isRequired,
  endDate: object.isRequired
};

export default inject(({store}) => {
  const {startDate, endDate, save} = store;
  return {startDate, endDate, save};
})(observer(Insert));
