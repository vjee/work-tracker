import React from "react";
import {object, func, string, array} from "prop-types";
import {inject, observer} from "mobx-react";
import DatePicker from "react-datepicker";
import styled from "styled-components";

import Row from "./../styles/Row";

const NumberInput = styled.input`min-width: 50px;`;

const TimeInputRow = ({date, title, target, placeholder, set}) =>
  <div>
    <h2>
      {title}
    </h2>
    <Row>
      <DatePicker
        dateFormat='DD/MM/YYYY'
        selected={date}
        onChange={date =>
          set(date, target === `start` ? `startDate` : `endDate`)}
      />
      <NumberInput
        type='number'
        min='0'
        max='23'
        placeholder={placeholder[0]}
        step='1'
        onChange={event =>
          set(event.target.value, target === `start` ? `startHour` : `endHour`)}
      />
      <NumberInput
        type='number'
        min='0'
        max='59'
        placeholder={placeholder[1]}
        step='15'
        onChange={event =>
          set(
            event.target.value,
            target === `start` ? `startMinutes` : `endMinutes`
          )}
      />
    </Row>
  </div>;

TimeInputRow.propTypes = {
  title: string.isRequired,
  date: object.isRequired,
  target: string.isRequired,
  placeholder: array.isRequired,
  set: func.isRequired
};

export default inject(({store}) => ({set: store.set}))(
  observer(TimeInputRow)
);
