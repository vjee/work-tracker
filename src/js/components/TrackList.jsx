import React from "react";
import {inject, observer, PropTypes} from "mobx-react";

import Track from "./Track";

const TrackList = ({tracks}) =>
  <section className='list'>
    {tracks.map(t => <Track key={t.key} data={t.value} />)}
  </section>;

TrackList.propTypes = {
  tracks: PropTypes.observableArray.isRequired
};

export default inject(({store}) => ({tracks: store.tracks}))(
  observer(TrackList)
);
