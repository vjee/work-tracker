import React from "react";
import {object} from "prop-types";
import moment from "moment";
import styled from "styled-components";

import humanize from "./../lib/humanizeMillis";
import Row from "./../styles/Row";

const Box = styled.div`
  border: 1px solid #ccc;
  padding: 5px;
  margin: 2px;
  width: 30px;
  text-align: right;
`;

const Track = ({data}) => {
  const start = moment(parseInt(data.start));
  const end = moment(parseInt(data.end));

  const millis = end.diff(start);
  const {hours, minutes} = humanize(millis);

  return (
    <Row>
      <Box>
        {hours}
      </Box>
      <Box>
        {minutes}
      </Box>
    </Row>
  );
};

Track.propTypes = {
  data: object.isRequired
};

export default Track;
