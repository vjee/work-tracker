import React from "react";

import "react-datepicker/dist/react-datepicker.css";

import TrackList from "./../components/TrackList";
import Insert from "./../components/Insert";

const App = () =>
  <div className='react-wrapper'>
    <header>
      <h1>Work Tracker</h1>
    </header>
    <main>
      <TrackList />
      <Insert />
    </main>
  </div>;

export default App;
